## About my code

I'm using a repository pattern as for what I believe and learned that it is good for encapsulating
the logic necessary to interact with our data with the following benifits.

- Centralize common data access functionality
- Better code maintainability
- Makes the code more testable

## Requirements

- PHP 7.3|^8.0
- Mysqlnd 8.0.7

## Setup
Run the following commands

- $ composer install
- $ php artisan migrate:fresh --seed


## Tasks

- TASK 1: Gets the list of popular movies
    
    Available Query params:
    - page (any int) (optional)
    - perPage (any int) (optional)
    - sort ('popularity') (optional)

    Route Example: http://127.0.0.1:8000/api/movies?page=1&perPage=10&sort=popularity

    Returns JSON response with pagination

- TASK 2: Gets the movie details by ID

    Route Example: http://127.0.0.1:8000/api/movies/1
