<?php

namespace App\Http\Controllers;

use App\Http\Resources\MovieResource;
use App\Repositories\Eloquent\MovieRepository;
use Illuminate\Http\Request;

class MovieController extends Controller
{

    private $movieRepository;

    public function __construct(MovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    public function index(Request $request)
    {
        $perPage = $request->input('perPage') ?? 10;
        $scopes = [];
        if ($request->input('sort') === 'popularity') {
            array_push($scopes, 'orderByRateAverage');
        }
        return MovieResource::collection($this->movieRepository->index($scopes, $perPage));
    }

    public function view(int $movieId)
    {
        return new MovieResource($this->movieRepository->findById($movieId, ['withRateAverage']));
    }
}
