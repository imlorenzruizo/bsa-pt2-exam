<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function scopeOrderByRateCount($query)
    {
        return $query->withCount('ratings')->orderBy('ratings_count', 'desc');
    }

    public function scopeOrderByRateAverage($query)
    {
        return $query->withAvg('ratings', 'rate')->orderBy('ratings_avg_rate', 'desc');
    }

    public function scopeWithRateAverage($query)
    {
        return $query->withAvg('ratings', 'rate');
    }
}
