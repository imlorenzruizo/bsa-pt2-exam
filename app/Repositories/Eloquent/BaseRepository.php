<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\EloquentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class BaseRepository implements EloquentRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all(array $columns = ['*'], array $relations = []): Collection
    {
        return $this->model->with($relations)->get($columns);
    }

    public function index(
        array  $scopes = [],
        int    $perPage = 10,
        array  $columns = ['*'],
        array  $relations = [],
        string $orderByColumn = 'updated_at',
        string $orderByDirection = 'asc'
    ): LengthAwarePaginator {
        $query = $this->model->select($columns)->with($relations);
        if (!empty($scopes)) {
            foreach ($scopes as $scope) {
                $query = $query->$scope();
            }
        }
        return $query->orderBy($orderByColumn, $orderByDirection)->paginate($perPage);
    }

    public function findById(int $modelId, array $scopes = [], array $columns = ['*'], array $relations = [], array $appends = [])
    {
        $query = $this->model->select($columns)->where('id', $modelId)->with($relations);

        if (!empty($scopes)) {
            foreach ($scopes as $scope) {
                $query = $query->$scope();
            }
        }

        return $query->firstOrFail();
    }
}
