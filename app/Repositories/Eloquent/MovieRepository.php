<?php

namespace App\Repositories\Eloquent;

use App\Models\Movie;
use App\Repositories\MovieRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

class MovieRepository extends BaseRepository implements MovieRepositoryInterface
{
    protected $model;

    public function __construct(Movie $model)
    {
        $this->model = $model;
    }
}
