<?php

namespace App\Repositories\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface EloquentRepositoryInterface
{
    public function all(array $columns = ['*'], array $relations = []): Collection;

    public function index(
        array $scopes = [],
        int $perPage = 10,
        array $columns = ['*'],
        array $relations = [],
        string $orderByColumn = 'updated_at',
        string $orderByDirection = 'asc'
    ): LengthAwarePaginator;

    public function findById(int $modelId, array $scopes = [], array $columns = ['*'], array $relations = [], array $appends = []);
}
