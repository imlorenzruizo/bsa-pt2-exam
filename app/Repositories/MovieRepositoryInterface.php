<?php

namespace App\Repositories;

use App\Repositories\Contracts\EloquentRepositoryInterface;

interface MovieRepositoryInterface extends EloquentRepositoryInterface
{
}
