<?php

namespace Database\Factories;

use App\Models\Movie;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RatingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rating::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::pluck('id')->toArray();
        $movies = Movie::pluck('id')->toArray();
        return [
            'user_id' => $this->faker->randomElement($users),
            'movie_id' => $this->faker->randomElement($movies),
            'rate' => $this->faker->biasedNumberBetween(0, 10),
        ];
    }
}
